export default function () {

    // Variables

    let isVideos            = $('div').is('.videos');
    let isResultSlider      = $('div').is('.result__slider');
    let isGallery           = $('div').is('.gallery');
    let isVideoGroup        = $('div').is('.video-group__slider');
    let isMinishop          = $('div').is('.minishop');
    let isChronicleSlider   = $('div').is('.chronicle__events--slider');
    let isVideoThumbSlider  = $('div').is('.videothumb__slider');
    let isStorySlider       = $('div').is('.stories__slider');
    let isTopSlider         = $('div').is('.top-block__slider');
    let isPartners          = $('div').is('.partners__slider');
    let isTeamSlider        = $('div').is('.team');
    let issuccessSlider     = $('div').is('.success__slider');
    let isPlayerContent     = $('div').is('.player-content__slider');
    let isProfileStat       = $('div').is('.profile-stat__slider');
    let isPlayersSlider     = $('div').is('.players__slider');
    let isPostGallery       = $('div').is('.post-gallery__slider');
    let isInformers         = $('div').is('.informers-slider');
    let isInformersTwo      = $('div').is('.informers-slider-two');
    let isSportType         = $('div').is('.sport-type-slider');
    let isMobileGallery     = $('div').is('.mobile-gallery-slider');

    // Mobile Nav
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.nav-mobile').toggleClass('open');
    });

    // Tabs

    $('.tab__nav--item').on('click', function(e){
        e.preventDefault();
        let tab = $(this).closest('.tab');
        let tabNav = $(this).closest('.tab__nav');
        let tabItem = '.' + $(this).attr('data-tab');

        tabNav.find('.tab__nav--item').removeClass('active');
        $(this).addClass('active');

        tab.find('.tab__item').removeClass('active');
        tab.find(tabItem).addClass('active');
    });

    $('.tabs__nav-item').on('click', function(e){
        e.preventDefault();
        let tabs = $(this).closest('.tabs');
        let tabsItem = '.' + $(this).attr('data-tab');

        tabs.find('.tabs__nav-item').removeClass('active');
        $(this).addClass('active');

        tabs.find('.tabs__item').removeClass('active');
        tabs.find(tabsItem).addClass('active');
    });

    $('.success__nav--item').on('click', function(e){
        e.preventDefault();

        let slide = $(this).attr('data-slide');
        success.slideTo(slide, 600);
    });

    // Videos block nav


    // videos Slider
    if (isVideos) {

        let videoListSlider = new Swiper('.active .video-list__slider',{
            loop: true,
            direction: 'vertical',
            slidesPerView: 'auto',
            spaceBetween: 14,
            freeMode: true,
            loopedSlides: 5,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });

        let videoMainSlider = new Swiper('.active .videos__slider',{
            slidesPerView: 1,
            spaceBetween: 20,
            loop: true,
            loopedSlides: 5,
            thumbs: {
                swiper: videoListSlider,
            },
        });

        $('.videos__header--nav-item').on('click', function(e){
            e.preventDefault();

            let tabs = $(this).closest('.videos');
            let tabsItem = '.tab' + $(this).attr('data-tab');
            let tabSlider = '.video-slider-' + $(this).attr('data-tab');

            $('.videos__header--nav-item').removeClass('active');
            $(this).addClass('active');

            videoMainSlider.destroy();
            videoListSlider.destroy();

            tabs.find('.videos__block').removeClass('active');
            tabs.find(tabsItem).addClass('active');

            videoListSlider = new Swiper('.active .video-list__slider',{
                loop: false,
                direction: 'vertical',
                slidesPerView: 'auto',
                spaceBetween: 14,
            })

            videoMainSlider = new Swiper('.active .videos__slider',{
                slidesPerView: 1,
                spaceBetween: 20,
                loop: true,
                loopedSlides: 5,
                thumbs: {
                    swiper: videoListSlider,
                },
            });
        });
    }

    $('.university__main--toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $('.university__main--text-hide').toggleClass('open');
    });

    $('.university__info--toggle').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $('.university__info--right').slideToggle('fast');
    });

    $( window ).resize(function() {
        if ($(window).width() > 1023) {
            $('.university__info--right').removeAttr('style');
        }
    });

    $('.datepicker-main').datepicker();

    $('.news-events__nav a').on('click', function(e){
        e.preventDefault();
        $('.news-events__nav a').removeClass('active');
        $(this).addClass('active');
    });

    $('.tribune__header--nav-item').on('click', function(e){
        e.preventDefault();
        $('.tribune__header--nav-item').removeClass('active');
        $(this).addClass('active');
    });

    $('.select').selectric({
        maxHeight: 240,
        disableOnMobile: false,
    });

    $('.select-lite').selectric({
        maxHeight: 240,
        disableOnMobile: false,
    });


    // Product Slider
    if (isMinishop) {
        let minishop = new Swiper('.minishop__slider',{
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 15,
            navigation: {
                nextEl: '.minishop__nav--next',
                prevEl: '.minishop__nav--prev',
            },
            scrollbar: {
                el: '.minishop__scrollbar',
            },
            breakpoints: {
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 37,
                },
            }
        });

        minishop.on('slideChange', function () {
            let sld = minishop.realIndex + 1;
            $('.minishop__counter span').text(sld);
        });
    }

    if (isVideoGroup) {
        let videoGroup = new Swiper('.video-group__slider',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.video-group__scrollbar',
            }
        })

        videoGroup.on('slideChange', function () {
            let sld = videoGroup.realIndex + 1;
            $('.video-group__counter--current').text(sld);
        });
    }


    /*
    $('.result-slider').owlCarousel({
        loop:true,
        margin:30,
        items:1,
        responsiveClass:true,
        responsive:{
            1024:{
                items:3,
            }
        }
    })

    $('.result__switch--item').on('click', function(e){
        e.preventDefault();
        let box = $(this).closest('.result');
        let tab = $($(this).attr('data-tab'));

        box.find('.result__switch--item').removeClass('active');
        $(this).addClass('active');

        box.find('.result__tab').removeClass('active');
        box.find(tab).addClass('active');

    });
    */
    // videoThumb Slider
    if (isVideoThumbSlider) {

        let videoThumb = new Swiper('.videothumb__slider',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.videothumb__nav--scrollbar',
            },
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                }
            }
        })

        videoThumb.on('slideChange', function () {
            let sld = videoThumb.realIndex + 1;
            $('.videothumb__nav--counter-value').text(sld);
        });

    }

    // Gallery Slider
    if (isGallery) {
        let gallery = new Swiper('.gallery__slider',{
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.gallery__scrollbar',
            }
        })

        gallery.on('slideChange', function () {
            let sld = gallery.realIndex + 1;
            $('.gallery__counter--current').text(sld);
        })
    }

    // chronicle Slider
    if (isChronicleSlider) {

        let chronicleSlider = new Swiper('.chronicle__events--slider',{
            loop: false,
            init: false,
            slidesPerView: 'auto',
            spaceBetween: 30,
            scrollbar: {
                el: '.chronicle__events--scrollbar',
            },
            on: {
                slideChange: function () {
                    let sld = chronicleSlider.realIndex + 1;
                    $('.chronicle__events--counter span').text(sld);
                },
            }
        })

        if ($(window).width() < 970) {
            chronicleSlider.init();
        }
    }


    // Story Slider
    if (isStorySlider) {
        let storySlider = new Swiper('.stories__slider',{
            loop: false,
            slidesPerView: 1,
            spaceBetween: 20,
            loopAdditionalSlides: 0
        })

        storySlider.on('slideChange', function () {
            let slide = '.story-' + storySlider.realIndex;
            $('.stories__nav--item').removeClass('active');
            $(slide).addClass('active');
        });

        $('.stories__nav--item').on('click', function(e){
            e.preventDefault();

            let slide = $(this).attr('data-slide');
            storySlider.slideTo(slide, 600);
            $('.stories__nav--item').removeClass('active');
            $(this).addClass('active');
        });
    }
    // top Slider
    if (isTopSlider) {
        let topSlider = new Swiper('.top-block__slider', {
            loop: true,
            spaceBetween: 16,
            pagination: {
                el: '.top-block__pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
            },
        });

    }

    // Partners Slider
    if (isPartners) {
        let partners = new Swiper('.partners__slider',{
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 0,
            scrollbar: {
                el: '.partners__scrollbar',
            },
            breakpoints: {
                1170: {
                    slidesPerView: 5
                },
            }
        });
    }

    if (issuccessSlider) {

        let success = new Swiper('.success__slider',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
        })
    }
    // Partners Slider
    if (isPlayerContent) {

        let PlayerContent = new Swiper('.player-content__slider',{
            loop: true,
            slidesPerView: 2,
            spaceBetween: 30,
            scrollbar: {
                el: '.player-content__scrollbar',
            },
            breakpoints: {
                567: {
                    slidesPerView: 3
                },
            }
        });

        PlayerContent.on('slideChange', function () {
            let sld = PlayerContent.realIndex + 1;
            $('.player-content__counter--current').text(sld);
        })
    }

    if (isProfileStat) {

        let ProfileStat = new Swiper('.profile-stat__slider',{
            loop: false,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.profile-stat__scrollbar',
            }
        });

        ProfileStat.on('slideChange', function () {
            let sld = ProfileStat.realIndex + 1;
            $('.profile-stat__counter--current').text(sld);
        })
    }

    if (isPlayersSlider)  {

        let PlayersStat = new Swiper('.players__slider',{
            loop: false,
            slidesPerView: 1,
            spaceBetween: 24,
            scrollbar: {
                el: '.players__scrollbar',
            },
            breakpoints: {
                1024: {
                    spaceBetween: 35,
                },
            }
        });

        PlayersStat.on('slideChange', function () {
            let sld = PlayersStat.realIndex + 1;
            $('.players__counter--current').text(sld);
        })
    }

    if (isPostGallery) {
        let PostGallery = new Swiper('.post-gallery__slider',{
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.post-gallery__scrollbar',
            }
        })

        PostGallery.on('slideChange', function () {
            let sld = PostGallery.realIndex + 1;
            $('.post-gallery__counter-value').text(sld);
        })
    }



    $('.faq__question').on('click', function(e){
        e.preventDefault();
        let faq = $($(this).closest('.faq'));
        faq.toggleClass('open')
        faq.find('.faq__answer').slideToggle('fast');
    });

    $('.abc__button').on('click', function(e){
        e.preventDefault();
        $(this).closest('.abc').toggleClass('open');
    });

    $('.abc ul li').on('click', function(e){
        e.preventDefault();
        $('.abc').find('li').removeClass('active');
        $(this).addClass('active');
        let abc_value = $(this).attr('data-value');
        $('.abc__button span').text(abc_value);
        $('.abc__input').val(abc_value);
        $('.abc').removeClass('open');
    });

    // Result Slider
    if (isInformers) {

        let informers = new Swiper('.informers-slider',{
            loop: false,
            init: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            scrollbar: {
                el: '.informers__scrollbar-one',
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                }
            }
        })

        informers.on('slideChange', function () {
            let sld = informers.realIndex + 1;
            $('.informers__counter--current').text(sld);
        });

    }

    if (isInformersTwo) {

        let informersTwo = new Swiper('.informers-slider-two',{
            loop: false,
            init: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            scrollbar: {
                el: '.informers__scrollbar-two',
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                1170: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                }
            }
        })

        informersTwo.on('slideChange', function () {
            let sld = informersTwo.realIndex + 1;
            $('.informers__counter--current-two').text(sld);
        });

    }

    if (isSportType) {

        let SportType = new Swiper('.sport-type-slider',{
            loop: false,
            init: true,
            simulateTouch: false,
            slidesPerView: 3,
            slidesPerColumn: 2,
            spaceBetween: 10,
            scrollbar: {
                el: '.sport-type__scrollbar',
            },
            breakpoints: {
                575: {
                    slidesPerView: 4,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                },
                768: {
                    slidesPerView: 5,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                },
                992: {
                    slidesPerView: 9,
                    slidesPerColumn: 2,
                    spaceBetween: 0,
                },
                1170: {
                    slidesPerView: 9,
                    slidesPerColumn: 2,
                    spaceBetween: 10,
                }
            }
        })

        SportType.on('slideChange', function () {
            let sld = SportType.realIndex + 1;
            $('.sport-type__counter--current').text(sld);
        });
    }

    let extra = new Swiper('.extra-slider',{
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 20,
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        }
    })

    $('.event-filter__header').on('click', function(e){
        e.preventDefault();
        $('.event-filter').toggleClass('open');
    });

    let partitions_main = new Swiper('.partitions__main',{
        loop: false,
        init: true,
        longSwipes: false,
        shortSwipes: false,
        simulateTouch: false,
        slidesPerView: 1,
        spaceBetween: 30,
        effect: 'fade',
        fadeEffect: {
            crossFade: true
        },
    });

    $('.partitions__nav--item').on('click', function(e){
        e.preventDefault();
        $(this).closest('.partitions__nav').find('.partitions__nav--item').removeClass('active');
        $(this).addClass('active');
        let tab = $(this).attr('data-nav');
        partitions_main.slideTo(tab, 600);
    });

    let isTournaments = $('div').is('.tournaments');

    if (isTournaments) {

        let tournaments_mobile_1 = new Swiper('.tournaments-mobile-1',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-1',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_1.on('slideChange', function () {
            let sld = tournaments_mobile_1.realIndex + 1;
            $('.tournaments-counter-1').text(sld);
        });

        let tournaments_mobile_2 = new Swiper('.tournaments-mobile-2',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-2',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_2.on('slideChange', function () {
            let sld = tournaments_mobile_2.realIndex + 1;
            $('.tournaments-counter-2').text(sld);
        });


        let tournaments_mobile_3 = new Swiper('.tournaments-mobile-3',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-3',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_3.on('slideChange', function () {
            let sld = tournaments_mobile_3.realIndex + 1;
            $('.tournaments-counter-3').text(sld);
        });


        let tournaments_mobile_4 = new Swiper('.tournaments-mobile-4',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 20,
            scrollbar: {
                el: '.tournaments-scrollbar-4',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                }
            }
        })

        tournaments_mobile_4.on('slideChange', function () {
            let sld = tournaments_mobile_4.realIndex + 1;
            $('.tournaments-counter-4').text(sld);
        });

    }


    // Calendar Modal
    $('.date-switch__button').on('click', function(e){
        e.preventDefault();
        $(this).closest('.date-switch').toggleClass('open');
    });

    // Hide dropdown
    $('body').on('click', function (event) {

        if ($(event.target).closest(".abc").length === 0) {
            $(".abc").removeClass('open');
        }

        if ($(event.target).closest(".collapsible-button").length === 0) {
            $(".collapsible-hidden").removeClass('open');
        }
    });



    $('.events-modal__scroll').mCustomScrollbar({
        theme: 'minimal-dark'
    });

    // Password field
    $('.field-icon-password').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('active');
    });

    // lineups block
    $('.lineups__header--item').on('click', function(e){
        e.preventDefault();
        let $tab = $($(this).attr('data-tab'));
        $('.lineups__header--item').removeClass('active');
        $(this).toggleClass('active');
        $('.lineups__command').removeClass('active');
        $tab.addClass('active');
    });

    // Gallery Slider
    if (isMobileGallery) {
        let mobileGllery = new Swiper('.mobile-gallery-slider',{
            loop: false,
            init: true,
            initialSlide: 0,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.mobile-gallery-scrollbar',
            }
        })

        mobileGllery.on('slideChange', function () {
            let sldmob = mobileGllery.realIndex + 1;
            $('.mobile-gallery-active').text(sldmob);
        })
    }


    // File Form
    (function() {

        $('.form-file input').on('change', function(e) {
            var box = $(this).closest('.form-file');
            var str = $(this).val();

            if (str.lastIndexOf('\\')){
                var i = str.lastIndexOf('\\')+1;
            }
            else{
                var i = str.lastIndexOf('/')+1;
            }
            var filename = str.slice(i);

            console.log(filename);

            box.find('.form-file__label').text(filename);


        });

    }());

    // switcher
    $('.switcher-button').on('click', function (e) {
        e.preventDefault();
        let box = $($(this).closest('.switcher'));
        let item = $(this).attr('href');
        box.find('.switcher-button').removeClass('active');
        $(this).addClass('active');

        box.find('.switcher-item').removeClass('active');
        box.find(item).addClass('active');
    })


    if (isTeamSlider) {

        let teamSlider1 = new Swiper('.team-slider-1',{
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.team-slider-scrollbar-1',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                }
            }
        })

        teamSlider1.on('slideChange', function () {
            let sldmob = teamSlider1.realIndex + 1;
            $('.team-counter-1').text(sldmob);
        })


        let teamSlider2 = new Swiper('.team-slider-2',{
            loop: true,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.team-slider-scrollbar-2',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                }
            }
        })

        teamSlider2.on('slideChange', function () {
            let sldmob = teamSlider2.realIndex + 1;
            $('.team-counter-2').text(sldmob);
        })
    }

    let experts = new Swiper('.experts-slider', {
        slidesPerView: 'auto',
        spaceBetween: 26,
    });

    let isPool     = $('div').is('.pool');
    if (isPool) {

        let pool = new Swiper('.pool-slider',{
            loop: false,
            init: true,
            slidesPerView: 1,
            spaceBetween: 30,
            scrollbar: {
                el: '.pool__scrollbar',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                }
            }
        })

        pool.on('slideChange', function () {
            let sld = pool.realIndex + 1;
            $('.pool__counter--current').text(sld);
        });

    }

    // Team Slider
    let team_list = new Swiper('.team-list__slider',{
        loop: false,
        slidesPerView: 'auto',
        spaceBetween: 18,
        scrollbar: {
            el: '.team-list__scrollbar',
        },
        breakpoints: {
            768: {
                spaceBetween: 30,
                slidesPerView: 'auto',
            },
            1170: {
                spaceBetween: 30,
                slidesPerView: 6
            },
        }
    });


    // store-tabs
    let storeTabsSlider = new Swiper('.active .store-tabs-slider',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 0,
        //slidesPerGroup: 1,
        navigation: {
            nextEl: '.store-tabs-next',
            prevEl: '.store-tabs-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
                //	slidesPerGroup: 2
            },
            1024: {
                slidesPerView: 3,
                //	slidesPerGroup: 3
            },
            1170: {
                slidesPerView: 4,
                //	slidesPerGroup: 4
            },
        },
        on: {
            slideChange: function () {

            },
        },
    });

    storeTabsSlider.on('slideChange', function () {
        let slideGroup = 1;
        let scr_w = document.body.clientWidth;
        if (scr_w >=  576) {
            slideGroup = 2;
        }
        if (scr_w >=  1024) {
            slideGroup = 3;
        }
        if (scr_w >=  1170) {
            slideGroup = 4;
        }
        let sld = storeTabsSlider.realIndex + slideGroup;
        $('.active .swiper-nav-current').text(sld);

    });

    $('.store-tabs-button').on('click', function(e){
        e.preventDefault();
        let block = $($(this).closest('.store-tabs'));
        let tab = $($(this).attr('href'));

        block.find('.store-tabs-button').removeClass('active');
        $(this).addClass('active');

        storeTabsSlider.destroy();

        block.find('.store-tabs-item').removeClass('active');
        tab.addClass('active');

        storeTabsSlider = new Swiper('.active .store-tabs-slider',{
            loop: false,
            slidesPerView: 1,
            spaceBetween: 0,
            //slidesPerGroup: 1,
            navigation: {
                nextEl: '.store-tabs-next',
                prevEl: '.store-tabs-prev',
            },
            scrollbar: {
                el: '.swiper-scrollbar',
            },
            breakpoints: {
                576: {
                    slidesPerView: 2,
                    //	slidesPerGroup: 2
                },
                1024: {
                    slidesPerView: 3,
                    //	slidesPerGroup: 3
                },
                1170: {
                    slidesPerView: 4,
                    //	slidesPerGroup: 4
                },
            },
            on: {
                slideChange: function () {

                },
            },
        });

        storeTabsSlider.on('slideChange', function () {
            let slideGroup = 1;
            let scr_w = document.body.clientWidth;
            if (scr_w >=  576) {
                slideGroup = 2;
            }
            if (scr_w >=  1024) {
                slideGroup = 3;
            }
            if (scr_w >=  1170) {
                slideGroup = 4;
            }
            let sld = storeTabsSlider.realIndex + slideGroup;
            $('.active .swiper-nav-current').text(sld);

        });

    })

    // hint
    let hintSlider = new Swiper('.hint-slider',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 0,
        navigation: {
            nextEl: '.hint-next',
            prevEl: '.hint-prev',
        },
        scrollbar: {
            el: '.swiper-scrollbar',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
            },
            1024: {
                slidesPerView: 3,
            },
            1170: {
                slidesPerView: 4,
            },
        }
    });

    hintSlider.on('slideChange', function () {
        let slideGroup = 1;
        let scr_w = document.body.clientWidth;
        if (scr_w >=  576) {
            slideGroup = 2;
        }
        if (scr_w >=  1024) {
            slideGroup = 3;
        }
        if (scr_w >=  1170) {
            slideGroup = 4;
        }
        let sld = hintSlider.realIndex + slideGroup;
        $('.hint-current').text(sld);

    });

    // Product gallery
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 25,
        slidesPerView: 'auto',
        loop: false,
        freeMode: true,
        //	loopedSlides: 5,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.thumbs-next',
            prevEl: '.thumbs-prev',
        },
    });
    var galleryTop = new Swiper('.product-gallery', {
        spaceBetween: 21,
        loop: false,
        //	loopedSlides: 5,
        thumbs: {
            swiper: galleryThumbs,
        },
    });

    // filter
    $('.sidebar-toggle').on('click', function(e){
        e.preventDefault();
        $('.sidebar').toggleClass('open');
    });


    // Collapsible Nav
    $(function () {

        let $btn = $('.collapsible-button');
        let $toggle = $('.collapsible-toggle');
        let $vlinks = $('.collapsible-links');
        let $hlinks = $('.collapsible-hidden-links');
        let $hblock = $('.collapsible-hidden');

        let numOfItems = 0;
        let totalSpace = 0;
        let closingTime = 1000;
        let breakWidths = [];

        // Get initial state
        $vlinks.children().outerWidth(function (i, w) {
            totalSpace += w;
            numOfItems += 1;
            breakWidths.push(totalSpace);
        });

        let availableSpace, numOfVisibleItems, requiredSpace, timer;

        function check() {

            // Get instant state
            availableSpace = $vlinks.width() - 10;
            numOfVisibleItems = $vlinks.children().length;
            requiredSpace = breakWidths[numOfVisibleItems - 1];

            // There is not enought space
            if (requiredSpace > availableSpace) {
                $vlinks.children().last().prependTo($hlinks);
                numOfVisibleItems -= 1;
                check();
                // There is more than enough space
            } else if (availableSpace > breakWidths[numOfVisibleItems]) {
                $hlinks.children().first().appendTo($vlinks);
                numOfVisibleItems += 1;
                check();
            }
            // Update the button accordingly
            $toggle.attr("count", numOfItems - numOfVisibleItems);
            if (numOfVisibleItems === numOfItems) {
                $toggle.addClass('hidden');
            } else $toggle.removeClass('hidden');
        }

        // Window listeners
        $(window).resize(function () {
            check();
        });

        $btn.click(function (e) {
            e.preventDefault();
            $hblock.toggleClass('open');
        });

        check();

    });

    $('.pretty-search-toggle').on('click', function(e){
        e.preventDefault();
        $('.search-mobile').toggleClass('open');
    });

    let result = function (){
        const isResult = document.querySelector('.result');
        if (isResult) {

            let resultSld = new Swiper('.active .result-slider',{
                loop: false,
                init: true,
                slidesPerView: 1,
                spaceBetween: 30,
                scrollbar: {
                    el: '.result__nav--scrollbar',
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                    }
                },
                on: {
                    init() {
                        $('.result__nav--total').text(this.slides.length);
                        let winWidth = $(window).width();
                        let slideTotal = this.realIndex + 1;
                        if (winWidth < 992 && this.slides.length < 2) {
                            $('.result__nav').addClass('hide');
                        }
                        if (winWidth > 992 && this.slides.length < 4) {
                            $('.result__nav').addClass('hide');
                        }
                    },
                    slideChange() {
                        let sld = this.realIndex + 1;
                        $('.result__nav--current').text(sld);
                    },
                },
            });

            $('.result__switch--item').on('click', function(e){
                e.preventDefault();
                let box = $(this).closest('.result');
                let tab = $($(this).attr('data-tab'));
                box.find('.result__switch--item').removeClass('active');
                $(this).addClass('active');
                box.find('.result__tab').removeClass('active');
                box.find(tab).addClass('active');
                $('.result__nav--current').text('1');

                resultSld.destroy();

                resultSld = new Swiper('.active .result-slider',{
                    loop: false,
                    init: true,
                    slidesPerView: 1,
                    spaceBetween: 30,
                    scrollbar: {
                        el: '.result__nav--scrollbar',
                    },
                    breakpoints: {
                        1024: {
                            slidesPerView: 3,
                            spaceBetween: 30,
                        }
                    },
                    on: {
                        init() {
                            $('.result__nav--total').text(this.slides.length);
                            let winWidth = $(window).width();
                            let slideTotal = this.realIndex + 1;
                            if (winWidth < 992 && this.slides.length < 2) {
                                $('.result__nav').addClass('hide');
                            }
                            if (winWidth > 992 && this.slides.length < 4) {
                                $('.result__nav').addClass('hide');
                            }
                        },
                        slideChange() {
                            let sld = this.realIndex + 1;
                            $('.result__nav--current').text(sld);
                        },
                    },
                });

            });
        }
    }
    result();

    //----------------
    //----------------
    let isSticky = document.querySelector('#sticky-block');
    if (isSticky) {
        let st_width = document.documentElement.clientWidth;
        if (st_width > 1169) {

            var a = document.querySelector('#sticky-block'), b = null, K = null, Z = 0, P = 0, N = 0;  // если у P ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента, если у N — нижний край дойдёт до нижнего края элемента. Может быть отрицательным числом
            window.addEventListener('scroll', Ascroll, false);
            document.body.addEventListener('scroll', Ascroll, false);
            function Ascroll() {
                var Ra = a.getBoundingClientRect(),
                    R1bottom = document.querySelector('#content-block').getBoundingClientRect().bottom;
                if (Ra.bottom < R1bottom) {
                    if (b == null) {
                        var Sa = getComputedStyle(a, ''), s = '';
                        for (var i = 0; i < Sa.length; i++) {
                            if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                                s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                            }
                        }
                        b = document.createElement('div');
                        b.className = "stop";
                        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                        a.insertBefore(b, a.firstChild);
                        var l = a.childNodes.length;
                        for (var i = 1; i < l; i++) {
                            b.appendChild(a.childNodes[1]);
                        }
                        a.style.height = b.getBoundingClientRect().height + 'px';
                        a.style.padding = '0';
                        a.style.border = '0';
                    }
                    var Rb = b.getBoundingClientRect(),
                        Rh = Ra.top + Rb.height,
                        W = document.documentElement.clientHeight,
                        R1 = Math.round(Rh - R1bottom),
                        R2 = Math.round(Rh - W);
                    if (Rb.height > W) {
                        if (Ra.top < K) {  // скролл вниз
                            if (R2 + N > R1) {  // не дойти до низа
                                if (Rb.bottom - W + N <= 0) {  // подцепиться
                                    b.className = 'sticky';
                                    b.style.top = W - Rb.height - N + 'px';
                                    Z = N + Ra.top + Rb.height - W;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = - Z + 'px';
                                }
                            } else {
                                b.className = 'stop';
                                b.style.top = - R1 +'px';
                                Z = R1;
                            }
                        } else {  // скролл вверх
                            if (Ra.top - P < 0) {  // не дойти до верха
                                if (Rb.top - P >= 0) {  // подцепиться
                                    b.className = 'sticky';
                                    b.style.top = P + 'px';
                                    Z = Ra.top - P;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = - Z + 'px';
                                }
                            } else {
                                b.className = '';
                                b.style.top = '';
                                Z = 0;
                            }
                        }
                        K = Ra.top;
                    } else {
                        if ((Ra.top - P) <= 0) {
                            if ((Ra.top - P) <= R1) {
                                b.className = 'stop';
                                b.style.top = - R1 +'px';
                            } else {
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                        }
                    }
                    window.addEventListener('resize', function() {
                        a.children[0].style.width = getComputedStyle(a, '').width
                    }, false);
                }
            }
        }
    }

};
