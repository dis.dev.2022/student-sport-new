export default () => {
    const tabsArray = document.querySelectorAll('[data-tabs]');

    if (tabsArray.length > 0) {
        tabsArray.forEach(el => {
            el.addEventListener('click', (e) => {});
        });


        const tabsNav = document.querySelectorAll('[data-tabs-nav]');

        tabsNav.forEach(el => {
            el.addEventListener('click', (e) => {
                const tabs = e.currentTarget.closest('[data-tabs]');
                const tabsNavs = tabs.querySelectorAll('[data-tabs-nav]');
                const tabsContent = tabs.querySelectorAll('[data-tabs-target]');
                const tabsPath = e.target.dataset.tabsNav;

                tabsNavs.forEach(elem => {
                    elem.classList.remove('active');
                });
                tabsContent.forEach(elem => {
                    elem.classList.remove('active');
                });
                tabs.querySelector(`[data-tabs-nav="${tabsPath}"]`).classList.add('active');
                tabs.querySelector(`[data-tabs-target="${tabsPath}"]`).classList.add('active');
            });
        });
    }

};
