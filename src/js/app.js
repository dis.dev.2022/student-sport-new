"use strict"
import Old from './_old.js';
import * as flsFunctions from "./helpers/functions.js"; // Полезные функции

window.addEventListener("load", function (e) {

    // Проверка поддержки webp
    flsFunctions.isWebp();

    // Добавление класса после загрузки страницы
    flsFunctions.addLoadedClass();

    // SVG IE11 support
    svg4everybody();

    // Modal Fancybox
    Fancybox.bind('[data-fancybox], .btn-modal', {
        autoFocus: false
    });

    Old();
});
