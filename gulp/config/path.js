// Получаем имя папки проекта
import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = `./build`; // Также можно использовать rootFolder
const srcFolder = `./src`;

export const path = {
	build: {
		js: `${buildFolder}/js/`,
		css: `${buildFolder}/css/`,
		html: `${buildFolder}/`,
		images: `${buildFolder}/img/`,
		fonts: `${buildFolder}/fonts/`,
		files: `${buildFolder}/files/`
	},
	src: {
		js: `${srcFolder}/js/app.js`,
		images: `${srcFolder}/img/**/*.{jpg,jpeg,png,gif,webp}`,
		svg: `${srcFolder}/img/**/*.svg`,
		scss: `${srcFolder}/scss/main.scss`,
		html: `${srcFolder}/*.njk`,
		fonts: `${srcFolder}/assets/fonts/**/*.*`,
		files: `${srcFolder}/assets/files/**/*.*`,
		iconsMono: `${srcFolder}/assets/icons/mono/*.svg`,
		iconsColor: `${srcFolder}/assets/icons/color/*.svg`,
	},
	watch: {
		js: `${srcFolder}/js/**/*.js`,
		scss: `${srcFolder}/scss/**/*.scss`,
		html: `${srcFolder}/**/*.njk`,
		images: `${srcFolder}/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}`,
		files: `${srcFolder}/assets/files/**/*.*`,
		iconsMono: `${srcFolder}/assets/icons/mono/*.svg`,
		iconsColor: `${srcFolder}/assets/icons/color/*.svg`,
	},
    libs: {
        js      : [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/svg4everybody/dist/svg4everybody.legacy.js',
            'node_modules/@fancyapps/ui/dist/fancybox.umd.js',
            'node_modules/swiper/swiper-bundle.js',
            'node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
            'node_modules/selectric/src/jquery.selectric.js',
            'node_modules/air-datepicker/dist/js/datepicker.js',
            'node_modules/owl.carousel/dist/owl.carousel.js',
        ],
        css     : [
            'node_modules/@fancyapps/ui/dist/fancybox.css',
            'node_modules/swiper/swiper-bundle.css',
            'node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
            'node_modules/air-datepicker/dist/css/datepicker.css',
            'node_modules/owl.carousel/dist/assets/owl.carousel.css',
        ]
    },
	clean: buildFolder,
	buildFolder: buildFolder,
	srcFolder: srcFolder,
	rootFolder: rootFolder,
	ftp: ``
}
