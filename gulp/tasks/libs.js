import concat from 'gulp-concat';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import cleanCss from 'gulp-clean-css'; // Сжатие CSS файла

export const libJs = () => {
    return app.gulp.src(app.path.libs.js)
        .pipe(concat('libs.js'))
        .pipe(app.gulp.dest(app.path.build.js))
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(
            uglify()
        )
        .pipe(app.gulp.dest(app.path.build.js))
}


export const libCss = () => {
    return app.gulp.src(app.path.libs.css)
        .pipe(concat('libs.css'))
        .pipe(app.gulp.dest(app.path.build.css))
        .pipe(cleanCss())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(app.gulp.dest(app.path.build.css))
}
